package services;

import model.Employee;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.List;

public class FileService {

    public void writeToTxtFile(List<Employee> employeeList) {
        PrintWriter pw;
        try {
            pw = new PrintWriter("EmployeeList.txt");
            for (int i = 0; i < employeeList.size(); i++) {
                pw.println(employeeList.get(i));
            }
            pw.println("End of file!");
            pw.close();

        } catch (Exception e) {
            System.out.println("Error Occured!");
        }
    }

    public void readFromTxtFile() {
        try {
            File file = new File("EmployeeList.txt");

            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null)
                JOptionPane.showMessageDialog(null, st);
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error Occured!");
        }
    }
}
