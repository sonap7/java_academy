package model;

public class Employee {
        private int id;
        private static int counter;
        private String fullname;
        private double salary;
        private String type;
        private String address;

        public Employee(String fullname, double salary, String type, String address) {
            counter++;
            id=counter;
            this.fullname = fullname;
            this.salary = salary;
            this.type = type;
            this.address = address;
        }

        public void calc(){
            if (type =="Manager"){
                salary = salary + ((2 * salary)/100);
            }
        }

        @Override
        public String toString() {
            return "model.Employee: {id " +id + '\'' +
                    ", fullname='" + fullname + '\'' +
                    ", salary=" + salary +
                    ", type='" + type + '\'' +
                    ", address='" + address + '\'' +
                    '}';
        }

    public String getFullname() {
        return fullname;
    }

    public double getSalary() {
        return salary;
    }

    public String getType() {
        return type;
    }

    public String getAddress() {
        return address;
    }
}

