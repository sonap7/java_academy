import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
//        Random random = new Random();
//        OptionalInt max_num = random.ints().limit(1000).max();
//        OptionalInt min_num = random.ints().limit(1000).min();
//        OptionalDouble avg_num = random.ints().limit(1000).average();
//        long sum_num = random.ints().limit(1000).count();
//
//        System.out.println(max_num.getAsInt());
//        System.out.println(min_num.getAsInt());
//        System.out.println(avg_num.getAsDouble());
//        System.out.println(sum_num);

//--------------  2nd way:-----------------------

        Random random = new Random();
        List<Integer> nums =random.ints().limit(10).sorted().boxed().collect(Collectors.toList());
        int count = (int) nums.stream().count();
        System.out.println(count);
        IntSummaryStatistics stats = nums.stream().mapToInt(i->i).summaryStatistics();
        System.out.println("Highest number in List : " + stats.getMax());
        System.out.println("Lowest number in List : " + stats.getMin());
        System.out.println("Sum of all numbers : " + stats.getSum());
        System.out.println("Average of all numbers : " + stats.getAverage());

    }
}
