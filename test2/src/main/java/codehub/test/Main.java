package codehub.test;

import codehub.db.Singleton;
import codehub.product.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();

        singleton.act();

        Creator creator = new Creator();
        List<AbstractProduct> products = new ArrayList<AbstractProduct>();
        AbstractProduct newProduct = creator.productionFactoryMethod(TypeOfProduct.SERVICE);
        AbstractProduct newProduct2 = creator.productionFactoryMethod(TypeOfProduct.PRODUCT);
        AbstractProduct newProduct3 = creator.productionFactoryMethod(TypeOfProduct.SUBSCRIPTION);
        AbstractProduct newProduct4 = creator.productionFactoryMethod(TypeOfProduct.SUBSCRIPTION);

        products.add(newProduct);
        products.add(newProduct2);
        products.add(newProduct3);
        products.add(newProduct4);

//        System.out.println("Products");
//        for (AbstractProduct ap : products){
//            if (ap instanceof Product){
//                System.out.println(ap);
//            }
//        }
//
//        System.out.println("Services");
//        for (AbstractProduct ap : products){
//            if (ap instanceof Service){
//                System.out.println(ap);
//            }
//        }
//
//        System.out.println("Subscription");
//        for (AbstractProduct ap : products){
//            if (ap instanceof Subscription){
//                System.out.println(ap);
//            }
//        }

//---------------------- 2nd way---------------------------------

        products.stream().filter(product->product instanceof Product).collect(Collectors.toList()).toString();

        for (AbstractProduct ap : products){
            if (ap instanceof Product){
                System.out.println("Products");
                System.out.println(ap);
            }
            else if (ap instanceof Service){
                System.out.println("Services");
                System.out.println(ap);
            }
            else {
                System.out.println("Subscription");
                System.out.println(ap);
            }

        }

    }
}
