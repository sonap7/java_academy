package model;

public class Employee {
    private static int id;
    private String name;
    private String address;
    //private static int counter;

    public Employee(String name, String address) {
        id++;
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return id + ", " + name + ", " + address;
    }
}
