package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Getter private long id;
    @Getter @Setter private String name;
    @Getter @Setter private String address;

}
