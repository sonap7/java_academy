package codehub.product;

import lombok.Data;

@Data
public class Subscription extends AbstractProduct {
    private String fromDate;
    private String toDate;
}
